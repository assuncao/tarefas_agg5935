#include <stdio.h>
#include <math.h>
#include <stdlib.h>


#define size 1024

float calculateMedia(float *num){
    static float sum = 0;
    static int cont = 0;
    if (num == NULL && cont != 0){
        fprintf(stderr, "\n\tmedia: %0.2f\n\n", sum/cont);
    }
    if (num != NULL){
        cont += 1;
        sum += *num;
        //fprintf(stderr, "%f\n", sum);
    }
    return sum;
}

float desvP(float *num, float *media){
    float dP, dif;
    static float pow2 = 0;
    static int cont = 0;
    if (num != NULL){
        dif = *num - *media;
        pow2 += pow(dif,2);
        cont += 1;
    }
    if (num == NULL && cont != 0) {
        dP = sqrt(pow2/cont);
        fprintf(stderr, "\n\tdesvio padrao dos dados: %0.2f\n\n", dP);
    }
    return dP;
}

void openFile(char *filename, int arg, float media){
    FILE *file;
    float num;

    if ((file = fopen(filename,"r")) == NULL ){
        fprintf(stderr, "Invalid file.\n");
        return;
    }

    while (!feof(file)) {
        fscanf(file,"%f ",&num);
        if (arg == 3) desvP(&num,&media);
        else calculateMedia(&num);
    }

    fclose(file);
    return;
}

int main(int argc, char *argv[]) {
    char *filename = argv[1];
    int i;
    float num, media;
    if (argc < 2) {
        fprintf(stderr, "Quantidade de input mínima é de dois\n");
        return 1;
    }
    if (argc == 3) media = atof(argv[2]);

    calculateMedia(NULL);
    desvP(NULL,NULL);
    if (argc == 2) openFile(filename,argc,media);
    else if (argc == 3) openFile(filename,argc,media);
    else if (argc > 2){
        for (i=1;i<argc;i+=1) {
            num = atof(argv[i]);
            calculateMedia(&num);
        }
    }
    desvP(NULL,NULL);
    calculateMedia(NULL);
    return 0;
}

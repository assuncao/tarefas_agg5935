#include <stdio.h>
#include <stdlib.h>

#define size 1024

void media(float *s){
    *s = *s + *s;
    fprintf(stderr, "%f\n", *s);
}

int main(int argc, char *argv[]) {
    FILE *file;
    char data[size];
    char *filename = argv[1];
    float soma = 0;

    media(&soma);

    file = fopen(filename, "r");
    while(!feof(file)){
        fgets(data,size,file);
        soma = atof(data);
        media(&soma);
        //fputs(data,stdout);
    }
    fclose(file);


    return 0;
}

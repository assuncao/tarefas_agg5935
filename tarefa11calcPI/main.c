#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char **argv)
{
    int n,n_th,soma_total=0;//,soma;
    double dt;
	
//	MPI_Init(&argc,&argv);
//	MPI_Comm_rank(MPI_COMM_WORLD,&ID);
//	MPI_Comm_size(MPI_COMM_WORLD,&Procs);
	
    n = atoi(argv[1]);
    n_th = atoi(argv[2]);
	
#   pragma omp parallel num_threads(n_th) reduction(+:soma_total)
    {
    int cont_dentro=0;
    float x,y;
    int i;
    int meu_th = omp_get_thread_num();
    int total_th = omp_get_num_threads();
    double tempo1=omp_get_wtime();
    // srand(meu_th);
    unsigned int seed = meu_th;
    // double t1,t2,dt;
    // double tempo_max;
	
    // t1 = MPI_Wtime();
	
    int n_local = n/total_th;
	
	for (i=0;i<n_local;i++){
        x = (float)(rand_r(&seed))/RAND_MAX;
        y = (float)(rand_r(&seed))/RAND_MAX;
		if (x*x+y*y<1) cont_dentro++;
	}

    //MPI_Reduce(&cont_dentro,&soma,1,MPI_INT,MPI_SUM, 0, MPI_COMM_WORLD);
    soma_total+=cont_dentro;
    //cont_dentro+=soma_total;
    double tempo2=omp_get_wtime();
    double dtempo=tempo2-tempo1;
    if (dtempo>dt) dt = dtempo;
    }
    printf("Tempo = %.10lf\n",dt);
    // if (meu_th==0) printf("Valor aproximado para pi: %f\n",(float)(soma)*4.0/n);
    printf("Valor aproximado para pi: %f\n",(float)(soma_total)*4.0/n);
    // t2 = MPI_Wtime();
	
    // dt = t2-t1;
	
    // MPI_Reduce(&dt,&tempo_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
	
    // if (meu_th==0) printf("%lf segundos\n", tempo_max);
	
    // MPI_Finalize();
	
	return 0;
	
}

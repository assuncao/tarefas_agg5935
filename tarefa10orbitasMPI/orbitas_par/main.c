#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <stddef.h>
#include "tools.h"

const double G = 6.67E-11;
const double ANO = 365.*24.*3600.;
const double UA = 149597870700.;

int main(int argc, char *argv[])
{
    Corpo *C1;
    FILE *fo;
    char ss[100];

    /* Number of bodies */
    long n = atoi(argv[1]);

    /* Alloc n structs */
    C1 = (Corpo *) malloc(sizeof(Corpo)*n);

    /* Variables */
    int ID, nproc;
    int n_local, n_begin;

    /* Auxiliary variables */
    int i;
    double t_start, t_finish, time;

    /* Open file */
    sprintf(ss,"orbitas.txt");
    fo = fopen(ss,"w");
    fprintf(fo,"%ld\n",n);

    /* MPI */
    MPI_Init(NULL,NULL);

    /* Execution time (start) */
    MPI_Barrier(MPI_COMM_WORLD);
    t_start = MPI_Wtime();

    MPI_Comm_rank(MPI_COMM_WORLD,&ID);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    /* Create MPI struct */
    MPI_Datatype ORBIT,types[1] = {MPI_DOUBLE};
    int blocks[1]={7};
    MPI_Aint displ[1];
    displ[0] = offsetof(Corpo,vx);
    MPI_Type_create_struct(1,blocks,displ,types,&ORBIT);
    MPI_Type_commit(&ORBIT);

    /* Time */
    double t=0,taux=0;
    double dt = ANO/10000;

    /* Intervals */
    n_local = n/nproc;
    n_begin = ID*n_local;

    /* Init */
    if (ID == 0){
        init(n,C1);
        print_orbita(fo, n, t, C1);
    }

    /* Broadcast all the structs to every ID */
    MPI_Bcast(C1,n,ORBIT,0,MPI_COMM_WORLD);

    for (t=dt, taux=dt; t<=ANO*1.0; t+=dt,taux+=dt){
        /* Run gravidade() for n_local-n_begin bodies */
        gravidade(n_begin,n_local+n_begin,n,dt,t,C1);

        /* Send for every other ID and receive from every other ID */
        for (i=0;i<nproc;i+=1) MPI_Bcast(&C1[i*n_local],n_local,ORBIT,i,MPI_COMM_WORLD);

        /* print_orbita() if ID */
        if (ID == 0 && taux>=ANO/20000){//200
            taux = 0.0;
            print_orbita(fo, n, t, C1);
        }
    }

    /* Execution time (finish) */
    t_finish = MPI_Wtime();
    time = t_finish - t_start;

    /* Time will be the highest for ID == 0 in this program */
    if (ID==0) printf("Time to calculate: %.10lf seconds.\n", time);

    /* Freeing */
    MPI_Type_free(&ORBIT);
    MPI_Finalize();
    free(C1);

    return (0);
}

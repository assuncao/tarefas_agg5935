#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <stddef.h>
#include "tools.h"

double randNorm()
{
    return(2.0*((double)rand()/RAND_MAX-0.5));
}

void print_orbita(FILE *fo, long n, double t, Corpo *C)
{
    long i;

    fprintf(fo,"%f ",t/ANO);

    for (i=0;i<n;i++){
        fprintf(fo,"%.3f %.3f ",C[i].px/UA,C[i].py/UA);
    }
    fprintf(fo,"\n");

}

void init(long n, Corpo *C){
    long i;

    double x,y;
    double ra, va;

    srand(1);

    for (i=0; i<n; i++){
        x = randNorm()*200E9;
        y = randNorm()*200E9;
        C[i].px = x;
        C[i].py = y;

        ra = sqrt(x*x+y*y);

        va = 30000.0*sqrt(150.E9/ra);

        C[i].vx = -va*y/ra;//randNorm()*10000;
        C[i].vy = va*x/ra;//randNorm()*10000;

        C[i].M = fabs(randNorm()*1E23);

    }

    C[0].vx = 0;
    C[0].vy = 0;
    C[0].px = 0;
    C[0].py = 0;
    C[0].M = 2.0E30;

    C[1].vx = 0;
    C[1].vy = 30000.0;
    C[1].px = UA;
    C[1].py = 0;
    C[1].M = 6.0E27;

    if (n>2){
        C[2].vx = 0;
        C[2].vy = 30000.0 + 6800.0;
        C[2].px = UA+400000000.0*15;
        C[2].py = 0;
        C[2].M = 7.0E22;
    }

}

void gravidade(long n_begin, long n_local, long n, double dt, double t, Corpo *C){

    long i,j;
    double ax,ay,dx,dy,raux,r3;

    // fprintf(stderr,"%d %d\n", n_begin,n_local);
    for (i=n_begin;i<n_local;i++){
        ax = 0;
        ay = 0;

        /* Calcultarion of the acelerations ax and ay */
        for (j=0;j<n;j+=1){
            if (i!=j){
                dx = C[i].px - C[j].px;
                dy = C[i].py - C[j].py;
                raux = sqrt(pow(dx,2)+pow(dy,2));
                r3 = pow(raux,3);
                ax += -dx*G*C[j].M/r3;
                ay += -dy*G*C[j].M/r3;
            }
        }

        if (t==dt){
            C[i].vx += ax*dt/2;
            C[i].vy += ay*dt/2;
        }
        else {
            C[i].vx += ax*dt;
            C[i].vy += ay*dt;
        }

        /* Update px2 and py2 values */
        C[i].px2 = C[i].px + C[i].vx*dt;
        C[i].py2 = C[i].py + C[i].vy*dt;

    }

    for (i=n_begin;i<n_local;i++){
        C[i].px=C[i].px2;
        C[i].py=C[i].py2;
    }
}

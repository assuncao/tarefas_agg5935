import numpy as np
import matplotlib.pyplot as plt

A = np.loadtxt("orbitas.txt",skiprows=1);

n,m = np.shape(A)

print(n,m)


for i in range(0,n,1):
	
	plt.figure(figsize=(10,10))
	
	p = A[i,1:]

	x,y = p.reshape(((m-1)/2,2)).T

	plt.plot(x,y,".")
	
	plt.xlim([-1.5,1.5])
	plt.ylim([-1.5,1.5])


	plt.savefig("fig_%04d" % (i))
	plt.close()


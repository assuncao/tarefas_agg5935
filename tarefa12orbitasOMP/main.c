#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "tools.h"

const double G = 6.67E-11;
const double ANO = 365.*24.*3600.;
const double UA = 149597870700.;

int main(int argc, char** argv)
{
	Corpo *C1;

    /* Number of bodies and threads */
    long n = atoi(argv[1]);
    long n_th = atoi(argv[2]);

    /* Alloc the structs */
	C1 = (Corpo *) malloc(sizeof(Corpo)*n);

    /* Auxiliary variables */
    double t_start, t_finish, time;

    /* Time */
	double t=0,taux=0;
	double dt = ANO/10000;

    /* Open file */
	FILE *fo;
	char ss[100];
	sprintf(ss,"orbitas.txt");
	fo = fopen(ss,"w");
	fprintf(fo,"%ld\n",n);

    /* Init */
    t_start = omp_get_wtime();
    init(n,C1);
	print_orbita(fo, n, t, C1);

	for (t=dt, taux=dt; t<=ANO*1.0; t+=dt,taux+=dt){

#       pragma omp parallel num_threads(n_th)
        gravidade(n,dt,t,C1,n_th);
        if (taux>=ANO/200) {
            taux=0.0;
            print_orbita(fo, n, t, C1);
        }
	}
    t_finish = omp_get_wtime();
    time = t_finish - t_start;
    fprintf(stderr,"Time to calculate: %.10lf seconds.\n",time);

    return (0);
}

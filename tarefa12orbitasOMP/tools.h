#ifndef TOOLS_H
#define TOOLS_H

extern const double G;
extern const double ANO;
extern const double UA;

typedef struct corpo{
    double vx;
    double vy;
    double px;
    double py;
    double px2;
    double py2;
    double M;
} Corpo;

double randNorm();
void init(long n, Corpo *C);
void print_orbita(FILE *fo, long n, double t, Corpo *C);
void gravidade(long n, double dt,double t, Corpo *C, long n_th);

#endif // TOOLS_H

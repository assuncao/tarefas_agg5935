#include <stdio.h>
#include <stdlib.h>
#include <mpi/mpi.h>

#define SIZE 1024

int main(int argc, char* argv[])
{
    int ID, nproc, numEle, numCol, i, numLin, dest, source, posic;
    int tag = 0;
	char name[SIZE];
    int *matrix;

	MPI_Init(&argc,&argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &ID);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    numLin = atoi(argv[1]);
    numCol = atoi(argv[2]);

    numEle = numLin*numCol;
    matrix = (int *) malloc (numEle*sizeof(int));

	FILE *file;
	sprintf(name,"%d.txt",ID);
	file = fopen(name,"w");

    fprintf(file, "A:\t");
    for (i=0;i<numEle;i+=1){
		matrix[i] = ID;
        if (i%numCol==0 && i!=0) fprintf(file,"\n\t");
        fprintf(file, "%d    ", matrix[i]);
	}
    fprintf(file, "\nA*:\t");

    if (ID%2==0){posic=(numLin-1)*numCol; source=ID+1; dest=ID+1;}
    else {posic=0; source=ID-1; dest=ID-1;}

    if (dest>=0 && dest<nproc) MPI_Sendrecv_replace(&matrix[posic],numCol,MPI_INT,dest,tag,source,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    if (ID%2!=0){posic=(numLin-1)*numCol; source=ID+1; dest=ID+1;}
    else {posic=0; source=ID-1; dest=ID-1;}

    if (dest>=0 && dest<nproc) MPI_Sendrecv_replace(&matrix[posic],numCol,MPI_INT,dest,tag,source,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    for (i=0;i<numEle;i+=1){
        if (i%numCol==0 && i!=0) fprintf(file,"\n\t");
        fprintf(file, "%d    ", matrix[i]);
    }
    fprintf(file,"\n");

    fclose(file);
    free(matrix);
	MPI_Finalize();

	return 0;
}

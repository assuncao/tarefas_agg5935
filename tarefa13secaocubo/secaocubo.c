#include <stdio.h>
#include <stdlib.h>
#include "methods.h"
#include <time.h>

int main (int argc, char *argv[])
{
    clock_t begin = clock();
    Profile *well = NULL;
//    well = (Profile *) malloc (INITSIZE * sizeof (Profile));

    // Reading variables
    char *file2readName = "./SL2013sv_0.5d-grd_v2.1/SL2013sv_25k-0.5d.mod";
    char *file2writeName = "profile.grd";
    char *inputFileName = "input.csv";
    int wellNum;

    // Pointers to functions
    void (*calcDistance)(Point, Profile *, int) = argv[1][1] == 'v' ? calculateDistanceVincenty : calculateDistanceHaversine;

    wellNum = calculateLength (&well, inputFileName);

    openInput (well, inputFileName);

    readData (file2readName, &*well, wellNum, calcDistance);

    saveData (file2writeName, well, wellNum);

    // End
    free(well);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    fprintf(stderr, "\nExecution time:\t%.10f seconds.\n\n", time_spent);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "methods.h"

// Calculate profile length
int calculateLength(Profile **well, char *inputFileName)
{
    FILE *inputFile;
    char line[SIZE];
    void *nread;
    int wellNum = 0;

    fprintf(stderr, "\nCalculating profile length with input file (%s)...\n", inputFileName);

    if ((inputFile = fopen (inputFileName, "r")) == NULL ){
        fprintf(stderr, "Failed to read input file (%s).\nInput file invalid.\n", inputFileName);
        return -1;
    }

    while (!feof (inputFile))
    {
        nread = fgets (line, SIZE, inputFile);
        if (nread == NULL) continue;
        else
        {
            wellNum += 1;
        }
    }

    *well = (Profile *) malloc (wellNum * sizeof (Profile));
    fprintf(stderr, "Profile length calculated.\n\n");

    return wellNum;
}

// Read input data to *profile
int openInput (Profile *well, char *inputFileName)
{
    FILE *inputFile;
    char line[SIZE];
    void *nread;

    // Auxiliary variables
    int wellNum = 0;

    fprintf(stderr, "Reading input file (%s)...\n", inputFileName);

    if ((inputFile = fopen (inputFileName, "r")) == NULL ){
        fprintf(stderr, "Failed to read input file (%s).\nInput file invalid.\n", inputFileName);
        return -1;
    }

    while (!feof (inputFile))
    {
        nread = fgets (line, SIZE, inputFile);
        if (nread == NULL) continue;
        else
        {
            well[wellNum] = splitInput (line);
            wellNum += 1;
        }
    }

    if (inputFile!=NULL) fclose (inputFile);

    fprintf (stderr, "Input file read.\n\n");

    return 1;
}

// Split lines for profile and initialize variables
Profile splitInput (char *line)
{
    Profile holder;
    char *split;
    int i;

    split = strtok (line, " ");
    for (i=0; i<NCOLSINPUT; i+=1) {
        switch (i) {
            case 0:
                holder.lon = atof (split);
                break;
            case 1:
                holder.lat = atof (split);
                break;
            case 2:
                holder.distance = atof (split);
                break;
            default:
                break;
        }
        split = strtok (NULL, " ");
    }

    // Initialize data
    for (i=0; i<ZSIZE; i+=1)
    {
        holder.AbsVs[i] = 0.;
        holder.depth[i] = ZSTART +  i * ZSPACING;
        holder.aux[i] = 0;
    }

    return holder;
}

// Read the data from the file
int readData (char *file2readName, Profile *well, int wellNum, void (*calcDistance)(Point, Profile *, int))
{
    Point point;
    FILE *file2read;
    char line[SIZE];
    void *nread;

    if ((file2read = fopen(file2readName,"r")) == NULL ){
        fprintf(stderr, "\nInvalid file to be read.\n");
        fprintf(stderr, "%s\n", file2readName);
        return -1;
    }

    fprintf(stderr, "Reading data file (%s), \ncalculating distances...\n", file2readName);

    while (!feof (file2read)) {
        nread = fgets (line, SIZE, file2read);
        if (nread == NULL) continue;
        if (line[0] == '#') continue;
        if (line[0] != '#'){
            point = splitLine (line);
            if (point.depth >= ZSTART || point.depth <= ZEND) calcDistance (point, &*well, wellNum);
        }
    }

    if (file2read!=NULL) fclose (file2read);

    fprintf (stderr, "The file (%s) has been read. \nThe distances have been calculated.\n",file2readName);

    return 0;
}

// Split the read line in a struct
Point splitLine (char *line){
    int i;
    char *split;
    Point holder;

    split = strtok(line," ");
    for (i=0; i<NCOLSDATA; i+=1) {
        switch (i) {
            case 0:
                holder.depth = atof(split);
                break;
            case 1:
                holder.lon = atof(split);
                break;
            case 2:
                holder.lat = atof(split);
                break;
            case 6:
                holder.AbsVs = atof(split);
                break;
            default:
                break;
        }
        split = strtok(NULL," ");
    }

    return holder;
}

// Calculate distance using the haversine function
void calculateDistanceHaversine (Point point, Profile *well, int wellNum)
{
    // Auxiliary variables
    int wCoord;

    double lat1, lat2, lon1, lon2, dLat, dLon, distance, sinLat, cosLat, sinLon;

    for (wCoord=0; wCoord<wellNum; wCoord+=1)
    {
        lat1 = point.lat * RAD;
        lat2 = well[wCoord].lat * RAD;
        lon1 = point.lon * RAD;
        lon2 = well[wCoord].lon * RAD;

        dLat = lat1 - lat2;
        dLon = lon1 - lon2;

        sinLat = sin(dLat/2.0) * sin(dLat/2.0);
        cosLat = cos(lat1) * cos(lat2);
        sinLon = sin(dLon/2.0) * sin(dLon/2.0);

        distance = 2.0 * RADIUS * asin (sqrt (sinLat + cosLat * sinLon));

        if (distance <= RDISTANCE)
        {
            // calcultate position j in the vector for each depth
            int zCoord = (int) (point.depth - ZSTART) / ZSPACING;
            float numerator = well[wCoord].aux[zCoord] * well[wCoord].AbsVs[zCoord] + point.AbsVs;
            float denominator = well[wCoord].aux[zCoord] + 1;
            well[wCoord].AbsVs[zCoord] = numerator / denominator;
            well[wCoord].aux[zCoord] += 1;
            //well[wCoord].depth[zCoord] = point.depth;
        }
    }

    return;
}

// Calculate distance using the Vincenty function
void calculateDistanceVincenty (Point point, Profile *well, int wellNum)
{
    double distance, lat1, lat2, lon1, lon2, dLon, term1, termA1, termB1, term2, termA2, termB2;
    int wCoord;

    for (wCoord=0; wCoord<wellNum; wCoord+=1)
    {
        lat1 = point.lat * RAD;
        lat2 = well[wCoord].lat * RAD;
        lon1 = point.lon * RAD;
        lon2 = well[wCoord].lon * RAD;
        dLon = lon2 - lon1;

        termA1 = cos (lat2) * sin (dLon) * cos (lat2) * sin (dLon);
        termB1 = (cos (lat1) * sin (lat2) - sin (lat1) * cos (lat2) * cos(dLon)) *
                 (cos (lat1) * sin (lat2) - sin (lat1) * cos (lat2) * cos(dLon));
        termA2 = sin (lat1) * sin (lat2);
        termB2 = cos (lat1) * cos (lat2) * cos (dLon);

        term1 = termA1 + termB1;
        term2 = termA2 + termB2;

        distance = 2.0 * RADIUS * atan2 (sqrt (term1), term2);

        if (distance <= RDISTANCE)
        {
            // calcultate position j in the vector for each depth
            int zCoord = (int) (point.depth - ZSTART) / ZSPACING;
            float numerator = well[wCoord].aux[zCoord] * well[wCoord].AbsVs[zCoord] + point.AbsVs;
            float denominator = well[wCoord].aux[zCoord] + 1;
            well[wCoord].AbsVs[zCoord] = numerator / denominator;
            well[wCoord].aux[zCoord] += 1;
            well[wCoord].depth[zCoord] = point.depth;
        }
    }

    return;
}

int saveData ( char *file2writeName, Profile *well, int wellNum)
{
    fprintf(stderr, "\nWriting profile to (%s).\n", file2writeName);

    // Auxiliary variables
    int wCoord, zCoord;

    FILE *file2write;
    if ((file2write = fopen (file2writeName,"wb")) == NULL ){
        fprintf (stderr, "Invalid file to be written.\n");
        return -1;
    }

    double minVs = well[0].AbsVs[0];
    double maxVs = well[0].AbsVs[0];

    for (wCoord=0; wCoord<wellNum; wCoord+=1)
    {
        for (zCoord=0; zCoord<ZSIZE; zCoord+=1)
        {
            if (minVs > well[wCoord].AbsVs[zCoord]) minVs = well[wCoord].AbsVs[zCoord];
            if (maxVs < well[wCoord].AbsVs[zCoord]) maxVs = well[wCoord].AbsVs[zCoord];
        }
    }

    fprintf(file2write, "DSAA\n");
    fprintf(file2write, "%d %d\n", wellNum, ZSIZE);                             // N. Wells, N. ZSIZE
    fprintf(file2write, "%f %f\n", well[0].distance, well[wellNum-1].distance); // xBegin, xEnd
    fprintf(file2write, "%d %d\n", ZEND, ZSTART);                               // zBegin, zEnd
    fprintf(file2write, "%lf %lf\n", minVs, maxVs);                             // vsMin, vsMax

    for (zCoord=0; zCoord<ZSIZE; zCoord+=1)
    {
        for (wCoord=0; wCoord<wellNum; wCoord+=1)
            fprintf(file2write, "%lf ", well[wCoord].AbsVs[zCoord]);

        fprintf(file2write, "\n");
    }

    if(file2write!=NULL) fclose(file2write);
    fprintf(stderr, "Writing procedure finished.\n");

    return 1;
}

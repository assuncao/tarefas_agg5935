import numpy as np
import matplotlib.pyplot as plt
import sys,re
f = open(sys.argv[1])
f.readline()
nx,ny = re.split("[ ]+", f.readline().strip())
xmin,xmax = re.split("[ ]+", f.readline().strip())
ymin,ymax = re.split("[ ]+", f.readline().strip())
zmin,zmax = re.split("[ ]+", f.readline().strip())
extent = map(float, [xmin, xmax, ymin, ymax])
print extent
dem = np.loadtxt(f)
f.close()
plt.imshow(dem, extent=extent, aspect="auto")
plt.show()

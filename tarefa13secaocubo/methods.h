#ifndef METHODS_H
#define METHODS_H

#define SIZE        1024    // Maximum number of characteris in a read line
#define INITSIZE    100     // Initial size of the profile mem allocation

#define NCOLSDATA   9       // Number of columns in the data file
#define NCOLSINPUT  3       // Number of columns in the input file

#define RDISTANCE   150     // (km) Maximum distance to be part of a well

#define RAD         (3.1415926536 / 180.)   // For convertion purpose
#define RADIUS      6371.    // Earth's radius
#define ZSTART      25      // (km)
#define ZEND        700     // (km)
#define ZSPACING    25      // (km)
#define ZSIZE       ((ZEND - ZSTART) / ZSPACING + 1)    // Number of vertical points

typedef struct PROFILE{
    double lon;
    double lat;
    double distance;
    double depth[ZSIZE];
    double AbsVs[ZSIZE];
    int aux[ZSIZE];
} Profile;

typedef struct POINT
{
    double lon;
    double lat;
    double depth;
    double AbsVs;
} Point;

// Prototypes
int calculateLength (Profile **well, char *inputFileName);
int openInput (Profile *well, char *inputFileName);
Profile splitInput (char *line);
int readData (char *file2readName, Profile *well, int wellNum, void (*calcDistance)(Point, Profile *, int));
Point splitLine (char *line);
void calculateDistanceHaversine (Point point, Profile *well, int wellNum);
void calculateDistanceVincenty (Point point, Profile *well, int wellNum);
int saveData (char *file2writeName, Profile *well, int wellNum);

#endif /*METHODS_H*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define size 1024

float readCol(char *data, int col){
    char *split;
    int i;
    float num;

    // Read serie's name
    if (data[0] == '>') {
        split = strtok(data," \n");
        while (split != NULL) {
            if (split[0] == '#'){
                fprintf(stderr, "\nA média da coluna %c da série %s é: ", col+64, split);
            }

            split = strtok(NULL," \n");
        }
    }
    // Read column number
    if (data[0] != '>') {
        split = strtok(data," ");
        for (i = 0;i < col;i += 1) { //&& split != NULL
            if (i == col-1) num = atof(split);
            split = strtok(NULL," ");
        }
    }
    return num;
}

int colNum(char letter){
    int l;
    l = letter - 64;
    return l;
}

int main(int argc, char *argv[]) {
    FILE *file;
    char data[size];
    void *nread;
    char *fileName = argv[1];
    int col = colNum(argv[2][2]);
    float media = 0;
    int i;

    if (argc!=3){
        fprintf(stderr, "\tO programa deve ser executado com a regra:\n\n\t\t./main <arquivode entrada> -c<coluna>.\n");
        return 1;
    }
    //Open File
    if ((file = fopen(fileName,"r"))==NULL) fprintf(stderr, "ARQUIVO INVÁLIDO.\n");
    //Read File
    while (!feof(file)) {
        nread = fgets(data,size,file);
        if (nread == NULL) {
            fprintf(stderr, "%0.2f\n", media);
            continue;
        }
        if (data[0] == '>') {
            i = 0;
            if (media != 0) fprintf(stderr, "%0.2f", media);
            readCol(data,col);
            media = 0;
        }
        if (data[0] != '>'){
            i += 1;
            media = (readCol(data,col) + media*(i-1)) / i;
        }
    }
    //Close File
    fprintf(stderr, "\n");
    if(file!=NULL) fclose(file);
    return 0;
}

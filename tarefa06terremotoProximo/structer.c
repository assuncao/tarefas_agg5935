#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "main.h"

#define LAT -23.559438
#define LON -46.734651
#define RADIUS 6371
#define RAD (3.1415926536 / 180)

int readone(FILE *fio, Earthquake *earthquake) {
	char line[1024];

	char *s_date, *s_id, *s_lat, *s_lon, *s_depth;

	if ((fgets(line, 1024, fio)) == NULL) return -1;
	if (line[0] == '#') return 0;

	s_id   = strtok(line, "|");
	s_date = strtok(NULL, "|");
	s_lat  = strtok(NULL, "|");
	s_lon  = strtok(NULL, "|");
	s_depth= strtok(NULL, "|");

	if (s_id == NULL || s_date == NULL || s_lat == NULL || s_lon == NULL || s_depth== NULL) return -2;

	earthquake->s_id   = strdup(s_id);
	earthquake->s_date = strdup(s_date);
	earthquake->lat    = atof(s_lat);
	earthquake->lon    = atof(s_lon);
	earthquake->depth  = atof(s_depth);

    //fprintf(stderr, "%s %s %f %f %f\n",earthquake->s_id, earthquake->s_date, earthquake->lat, earthquake->lon, earthquake->depth);

	return 1;
}

void printEarthquake(Earthquake earthquake){
	fprintf(stderr, "%s %s %f %f %f\n",earthquake.s_id, earthquake.s_date, earthquake.lat, earthquake.lon, earthquake.depth);
	return;
}

// a função storeDistance abaixo foi adaptada de https://rosettacode.org/wiki/Haversine_formula#C
double storeDistance(Earthquake *earthquake){

    double dx, dy, dz, distance, phi1, phi2, theta1, theta2;

    phi1 = earthquake->lat;
    phi2 = LAT;
    theta1 = earthquake->lon;
    theta2 = LON;

	phi1 -= phi2;
	phi1 *= RAD;
    theta1 *= RAD;
    theta2 *= RAD;

	dz = sin(theta1) - sin(theta2);
	dx = cos(phi1) * cos(theta1) - cos(theta2);
	dy = sin(phi1) * cos(theta1);
	distance =  asin(sqrt(dx * dx + dy * dy + dz * dz) / 2) * 2 * RADIUS;

    //fprintf(stderr, "Distance from IAG: %lf\n", distance);

    return distance;
}

void printClosest(Earthquake *closest, double *distance){
    fprintf(stderr, "\nInformation from the closest earthquake to IAG \n\tid:\t%s \n\tdate:\t%s \n\tlat:\t%f degrees \n\tlon:\t%f degrees \n\tdepth:\t%f km \n\tdist.:\t%lf km\n\n", closest->s_id, closest->s_date, closest->lat, closest->lon, closest->depth, *distance);
    return;
}

#ifndef MAIN_H
#define MAIN_H

typedef struct EARTHQUAKE{
	char *s_id;
	char *s_date;
	float lat;
	float lon;
	float depth;
} Earthquake;

/* Prototypes for the functions */
int readone(FILE *fio, Earthquake *earthquake);
void printEarthquake(Earthquake earthquake);
double storeDistance(Earthquake *earthquake);
void printClosest(Earthquake *closest, double *distance);

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"

/*
 * Programa para ler parte da saida de um servidor FDSNws em modo texto, pode testar da seguinte maneira:
 *
 * wget -q -O - 'http://www.moho.iag.usp.br/fdsnws/event/1/query?format=text&limit=10' | main
 */

int main (int argc, char **argv) {
	double distance = 50000, dis;
	int verifier = 0;
	Earthquake earthquake, closest;

	fprintf(stderr, "\n");

	while (verifier >= 0){
		verifier = readone(stdin,&earthquake);
		if (verifier == 1) {
			printEarthquake(earthquake);
			dis = storeDistance(&earthquake);
			if (dis < distance){
				distance = dis;
				closest = earthquake;
			}
		}
	}

	printClosest(&closest,&distance);

	return 0;
}

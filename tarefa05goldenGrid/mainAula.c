#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Grid {
    char id[4];
    short nx;
    short ny;
    double xlo;
    double xhi;
    double ylo;
    double yhi;
    double zlo;
    double zhi;
} grid;

// Entender como usar o typedef na leitura
// Pesquisar função parse()
typedef struct Golden {
    grid h;
    float *v;
} golden;

int main(int argc, char *argv[]) {
    FILE *file;
    FILE *textfile;
    char *filename = argv[1];
    char *textfilename = "grid.txt";
    struct Grid buffer;
    float z;
    int i,j;
    float *h;

    if ((file = fopen(filename, "rb")) == NULL) {
        fprintf(stderr, "Arquivo inválido.\n");
        return 1;
    }
    if ((textfile = fopen(textfilename,"w")) == NULL) {
        fprintf(stderr, "Arquivo inválido.\n");
        return 1;
    }

    fread(&buffer,sizeof(buffer),1,file);

    fprintf(textfile, "%lf %hi %lf\n", buffer.xlo, buffer.nx, (buffer.xhi-buffer.xlo)/buffer.nx);
    fprintf(textfile, "%lf %hi %lf\n", buffer.ylo, buffer.ny, (buffer.yhi-buffer.ylo)/buffer.ny);

    h = (float *) malloc(buffer.nx*buffer.ny;
    fread(&h,sizeof(h),1,file);
    //for (i=0;i<buffer.nx;i+=1){
         //fread(&h[i],sizeof(float),1,file);
         //fprintf(textfile, "%f ", h[i]);
    //}
    //fprintf(stderr, "%f\n", h);
    free(h);
    // for (j=0;j<buffer.ny;j+=1){
    //     for (i=0;i<buffer.nx;i+=1){
    //         fread(&z,sizeof(float),1,file);
    //         fprintf(textfile, "%f ", z);
    //     }
    //     fprintf(textfile, "\n");
    // }
    fclose(textfile);
    fclose(file);
    return 0;
}

import numpy as np
import matplotlib.pyplot as plt
import sys

file = open(sys.argv[1])

xmin, nx, dx = file.readline().split(" ")
xmin = float(xmin)
nx   = int(nx)
dx   = float(dx)
xmax = xmin+nx*dx

ymin, ny, dy = file.readline().split(" ")
ymin = float(ymin)
ny   = int(ny)
dy   = float(dy)
ymax = ymin+ny*dy

m = np.loadtxt(file)
file.close()

plt.imshow(m, extent = (xmin, xmax, ymin, ymax), origin = 'lower')
plt.colorbar()
plt.show()


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Grid {
    char id[4];
    short nx;
    short ny;
    double xlo;
    double xhi;
    double ylo;
    double yhi;
    double zlo;
    double zhi;
};

int main(int argc, char *argv[]) {
    FILE *file;
    FILE *textfile;
    char *filename = argv[1];
    char *textfilename = "grid.txt";
    struct Grid buffer;
    int i,j;
    float **h = NULL;

    if ((file = fopen(filename, "rb")) == NULL) {
        fprintf(stderr, "Arquivo inválido.\n");
        return 1;
    }
    if ((textfile = fopen(textfilename,"w")) == NULL) {
        fprintf(stderr, "Arquivo inválido.\n");
        return 1;
    }

    fread(&buffer,sizeof(buffer),1,file);

    fprintf(textfile, "%lf %hi %lf\n", buffer.xlo, buffer.nx, (buffer.xhi-buffer.xlo)/buffer.nx);
    fprintf(textfile, "%lf %hi %lf\n", buffer.ylo, buffer.ny, (buffer.yhi-buffer.ylo)/buffer.ny);

    // Malloc
    h = (float **)malloc(buffer.nx*sizeof(float *));
    for (i=0;i<buffer.nx;i+=1){
        h[i] = (float *)malloc(buffer.ny*sizeof(float));
    }
    // Read to matrix
    for (j=0;j<buffer.ny;j+=1){
        for (i=0;i<buffer.nx;i+=1){
            fread(&h[i][j],sizeof(float),1,file);
            fprintf(textfile, "%f ", h[i][j]);
        }
        fprintf(textfile, "\n");
    }
    // Free
    for (i=0;i<buffer.nx;i+=1){
        free(h[i]);
    }
    free(h);

    fclose(textfile);
    fclose(file);
    return 0;
}

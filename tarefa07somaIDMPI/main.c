#include <stdio.h>
#include <mpi.h>

int main(int argc, char const *argv[]) {

    int ID, nproc, IDimpar, somaTotal, somaParcial;
    int tag = 0;

    MPI_Init(NULL,NULL);
    MPI_Comm_rank(MPI_COMM_WORLD,&ID);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    if (nproc > 3 || nproc % 2 != 0){
        if (ID % 2 == 1){
            IDimpar = ID;
            MPI_Send(&IDimpar,1,MPI_INT,ID-1,tag,MPI_COMM_WORLD);
            fprintf(stderr, "processo (%d) manda valor (%d)\n", ID, IDimpar);
        }
        else if (ID == 0){
            MPI_Recv(&IDimpar,1,MPI_INT,ID+1,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            MPI_Recv(&somaParcial,1,MPI_INT,ID+2,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            somaTotal = IDimpar + somaParcial;
            fprintf(stderr, "Soma dos ID's : %d\n", somaTotal);
        }
        else if (ID % 2 == 0) {
            if (ID == nproc - 2) {
                MPI_Recv(&IDimpar,1,MPI_INT,ID+1,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
                somaParcial = IDimpar + ID;
                fprintf(stderr, "processo (%d) manda valor (%d)\n", ID, somaParcial);
            }
            else {
                MPI_Recv(&IDimpar,1,MPI_INT,ID+1,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
                MPI_Recv(&somaParcial,1,MPI_INT,ID+2,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
                somaParcial = IDimpar + ID + somaParcial;
                fprintf(stderr, "processo (%d) manda valor (%d)\n", ID, somaParcial);
            }
            MPI_Send(&somaParcial,1,MPI_INT,ID-2,tag,MPI_COMM_WORLD);
        }
    }
    else {
        fprintf(stderr, "Número de processos inválido. É necessário um número par de processo maior ou igual à 4.\n");
        return -1;
    }
    MPI_Finalize();
    return 0;
}

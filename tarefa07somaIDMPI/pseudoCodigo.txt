int ID, nproc, IDimpar;
ID = getID();
nproc = nProc();

if (nproc >= 4) {
	if (ID % 2 == 1){
	    IDimpar = ID; // Eu posso mandar simplismente ID, mas fica confuso
	    send(ID - 1,IDimpar);
	}

	if ((ID % 2 == 0) && (ID != 0)){
	    if (ID == nproc - 2)
		recv(ID + 1,IDimpar);
		somaParcial = IDimpar + ID;
	    else
		recv(ID + 1,IDimpar);
		recv(ID + 2,somaParcial);
		somaParcial = somaParcial + IDimpar + ID;
	    send(ID - 2,somaParcial);
	}

	if (ID == 0){
	    recv(1,IDimpar);
	    recv(2,somaParcial);
	    somaTotal = IDimpar + somaParcial;
	}
}

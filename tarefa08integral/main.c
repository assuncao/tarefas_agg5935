#include <stdio.h>
#include <mpi.h>
#include <math.h>

#define PI 3.141592653589793
#define RES 2048

double f(double x){
    return cos(x);
}

double trap(double a, double b, int n, double dx){
    double soma = (f(a)+f(b))/2;

    for (int i=1;i<n-1;i+=1){
        soma+=f(a+i*dx);
    }
    soma*=dx;
    return soma;
}

int main(int argc, char const *argv[]) {
    double a = 0, b = PI;
    int ID, nproc;

    MPI_Init(NULL,NULL);
    MPI_Comm_rank(MPI_COMM_WORLD,&ID);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    int n_aux = RES / nproc + 1; // Acha n_aux tal que n >= RES
    int n = n_aux * nproc + 1;
    double dx = (b - a) / (n - 1);

    int nlocal = (n - 1) / nproc + 1;
    double local_a = a + dx*ID*(nlocal - 1), local_b = local_a + dx*(nlocal - 1);
    double soma_total;

    double soma = trap(local_a,local_b,nlocal,dx);

    MPI_Allreduce(&soma,&soma_total,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD); // sem target, todos recebem e somam (MPI_SUM)
    if (ID == 0) fprintf(stderr, "A integral vale (%0.1lf) \n", soma_total);

    MPI_Finalize();

    return 0;
}
